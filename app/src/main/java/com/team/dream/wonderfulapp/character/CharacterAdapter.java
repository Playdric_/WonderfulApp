package com.team.dream.wonderfulapp.character;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import com.team.dream.wonderfulapp.R;
import com.team.dream.wonderfulapp.model.Character;
import com.team.dream.wonderfulapp.utils.CircleTransform;

import java.util.ArrayList;
import java.util.List;

public class CharacterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    /***********************************************************
     *  Constants
     **********************************************************/
    private static final String TAG = "CharacterAdapter";
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    /***********************************************************
     *  Attributes
     **********************************************************/
    private List<Character> data = new ArrayList<>();
    private List<Character> dataFiltered = new ArrayList<>();
    private Activity activity;
    private OnBottomReachedListener onBottomReachedListener;
    private boolean isLoading;
    private boolean shouldListenToOnBottomReached = true;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    /***********************************************************
     *  Business Methods
     **********************************************************/
    public CharacterAdapter(Activity activity) {
        this.activity = activity;
        RecyclerView mRecyclerView = ((CharacterActivity) activity).rcvCharacters;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold) && shouldListenToOnBottomReached) {
                    if (onBottomReachedListener != null) {
                        onBottomReachedListener.onBottomReached();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void resetData(List<Character> items) {
        this.dataFiltered = items;
        this.data = items;
        notifyDataSetChanged();
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener) {
        this.onBottomReachedListener = onBottomReachedListener;
    }


    public void setLoaded() {
        isLoading = false;
    }

    public void setShouldListenToOnBottomReached(boolean b) {
        shouldListenToOnBottomReached = b;
    }

    /***********************************************************
     *  Adapter stuff
     **********************************************************/
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CharacterViewHolder) {
            final CharacterViewHolder viewHolder = (CharacterViewHolder) holder;
            //Shared Preferences editor, to put the boolean when checked
            String prefName = FirebaseAuth.getInstance().getCurrentUser().getEmail();
            final SharedPreferences.Editor editor = activity.getSharedPreferences(prefName, Context.MODE_PRIVATE).edit();
            final String characterName = dataFiltered.get(position).getName();
            final String imgUrl = dataFiltered.get(position).getImage();
            final ImageView imvCharacter = viewHolder.imageView;

            viewHolder.textView.setText(characterName);

            Picasso.get()
                    .load(imgUrl)
                    .transform(new CircleTransform())
                    .error(R.drawable.ic_broken_image_black_24dp)
                    .placeholder(R.drawable.ic_image_black_24dp)
                    .into(imvCharacter);

            //Retrieving the state (checked) for the current item
            SharedPreferences prefs = activity.getSharedPreferences(prefName, Context.MODE_PRIVATE);
            boolean checked = prefs.getBoolean(characterName, false);
            viewHolder.ckbCharacter.setChecked(checked);

            viewHolder.ckbCharacter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean checked = ((CheckBox) v).isChecked();
                    editor.putBoolean(dataFiltered.get(position).getName(), checked);
                    editor.apply();
                    Log.d(TAG, checked ? "onClick: checked " : "onClick: unchecked " + characterName);
                }
            });
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            if (loadingViewHolder.progressBar == null) {
            } else {
                loadingViewHolder.progressBar.setIndeterminate(true);
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataFiltered == null ? 0 : dataFiltered.size();
    }

    @Override
    public int getItemViewType(int position) {
        return dataFiltered != null ? dataFiltered.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM : 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity.getBaseContext()).inflate(R.layout.item_character, parent, false);
            return new CharacterViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity.getBaseContext()).inflate(R.layout.loading_item, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    /***********************************************************
     *  Holder classes
     **********************************************************/
    class CharacterViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        ImageView imageView;
        CheckBox ckbCharacter;

        CharacterViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tvTitle);
            imageView = itemView.findViewById(R.id.imv_character);
            ckbCharacter = itemView.findViewById(R.id.ckb_character);
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.rcvProgressBar);
        }
    }

    /***********************************************************
     *  Override from Filterable
     **********************************************************/
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    dataFiltered = data;
                } else {
                    List<Character> filteredList = new ArrayList<>();
                    for (Character row : data) {
                        if (row != null && row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    dataFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                dataFiltered = (ArrayList<Character>) results.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}
