package com.team.dream.wonderfulapp.crosslayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.team.dream.wonderfulapp.R;
import com.team.dream.wonderfulapp.character.CharacterActivity;
import com.team.dream.wonderfulapp.episode.EpisodeActivity;
import com.team.dream.wonderfulapp.profile.ProfileActivity;
import com.team.dream.wonderfulapp.randomquote.RandomQuoteActivity;

public abstract class BaseActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static final String BASE_URL = "https://rickandmortyapi.com/api/";

    protected BottomNavigationView navigationView;
    public  abstract int getLayoutId();
    public abstract int getNavigationMenuItemId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        navigationView = findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateNavigationBarState();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.navigation_character:
                startActivity(new Intent(this, CharacterActivity.class));
                break;
            case R.id.navigation_episode:
                startActivity(new Intent(this, EpisodeActivity.class));
                break;
            case R.id.navigation_random_quote:
                startActivity(new Intent(this, RandomQuoteActivity.class));
                break;
            case R.id.navigation_profile:
                startActivity(new Intent(this, ProfileActivity.class));
            default:
                break;
        }

        finish();
        return true;
    }


    /**
     * ça va permettre de pas voir l'animation de base au passage d'une activity à une autre
     */
    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0,0);
    }

    private void updateNavigationBarState(){
        int actionId = getNavigationMenuItemId();
        selectBottomNavigationBarItem(actionId);
    }

    private void selectBottomNavigationBarItem(int itemId){
        Menu menu = navigationView.getMenu();
        for(int i = 0, size = menu.size(); i < size; i++){
            MenuItem item = menu.getItem(i);
            if(item.getItemId() == itemId){
                item.setChecked(true);
                break;
            }
        }
    }
}
