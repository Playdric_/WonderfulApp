package com.team.dream.wonderfulapp.randomquote;

import android.content.Context;
import android.support.annotation.NonNull;

import com.team.dream.wonderfulapp.Constants;
import com.team.dream.wonderfulapp.model.Quote;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class RandomQuotePresenter implements IRandomQuotePresenter {

    @NonNull
    private IRandomQuoteView mainView;

    @NonNull
    private Context context;

    RandomQuotePresenter(@NonNull IRandomQuoteView mainView, @NonNull Context context) {
        this.mainView = mainView;
        this.context = context;
    }

    @Override
    public List<Quote> loadJsonQuotes() {
        String json = null;
        List<Quote> quoteList = new ArrayList<>();

        try {
            InputStream is = context.getAssets().open("rick_and_morty.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        try {
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray(Constants.PARAMETERS_JSON_ARRAY_QUOTES);

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                Quote quote = new Quote();
                quote.setId(jo_inside.getString(Constants.PARAMETERS_JSON_QUOTE_ID));
                quote.setWhat(jo_inside.getString(Constants.PARAMETERS_JSON_QUOTE_WHAT));
                quote.setWhere(jo_inside.getString(Constants.PARAMETERS_JSON_QUOTE_WHERE));
                quote.setWhen(jo_inside.getString(Constants.PARAMETERS_JSON_QUOTE_WHEN));
                quote.setWho(jo_inside.getString(Constants.PARAMETERS_JSON_QUOTE_WHO));


                //Add your values in your `ArrayList` as below:
                quoteList.add(quote);
            }
            return quoteList;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
