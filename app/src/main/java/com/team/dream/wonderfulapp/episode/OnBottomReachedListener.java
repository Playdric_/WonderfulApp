package com.team.dream.wonderfulapp.episode;

/**
 * Created by Florian on 08/05/2018.
 */

public interface OnBottomReachedListener {
    void onBottomReached();
}
