package com.team.dream.wonderfulapp.character;

import com.team.dream.wonderfulapp.model.Character;

import java.util.ArrayList;
import java.util.List;

public interface ICharacterPresenter {

    /**
     * query all the user from the API
     */
    void getUser();

    ArrayList<Character> favList(List<Character> list);
}
