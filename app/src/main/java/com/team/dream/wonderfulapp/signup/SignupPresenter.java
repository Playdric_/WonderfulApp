package com.team.dream.wonderfulapp.signup;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.team.dream.wonderfulapp.Constants;
import com.team.dream.wonderfulapp.R;

public class SignupPresenter implements ISignupPresenter {

    /***********************************************************
    *  Constants
    **********************************************************/
    private static final String TAG = "SignupPresenter";

    /***********************************************************
    *  Attributes
    **********************************************************/
    private FirebaseAuth mAuth;
    private FirebaseAnalytics firebaseAnalytics;
    private Context context;

    @NonNull
    private ISignupView signupView;

    /***********************************************************
    *  Constructors
    **********************************************************/
    public SignupPresenter(@NonNull ISignupView signupView, Context context) {
        this.signupView = signupView;
        mAuth = FirebaseAuth.getInstance();
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        this.context = context;
    }

    /***********************************************************
     *  Business Methods
     **********************************************************/
    private void createUser(final String name, final String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        signupView.updateButtonState(true);
                        signupView.setPrbVisible(View.INVISIBLE);
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUser: user created successfully");
                            saveDisplayName(name);

                            Bundle params = new Bundle();
                            params.putString("email", email);
                            firebaseAnalytics.logEvent("SIGNUP", params);
                        } else {
                            signupView.setPrbVisible(View.INVISIBLE);
                            Log.d(TAG, "createUser: failed to create user");
                            if (task.getException() != null) {
                                Log.d(TAG, "createUser: error: " + task.getException().getMessage());
                                signupView.showToasty(task.getException().getMessage());
                            }

                            Bundle params = new Bundle();
                            params.putString("email", email);
                            firebaseAnalytics.logEvent("SIGNUP", params);
                        }
                    }
                });
    }

    private void saveDisplayName(String name) {
        UserProfileChangeRequest changeRequest = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build();
        FirebaseUser user = mAuth.getCurrentUser();

        if (user != null) {
            user.updateProfile(changeRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "saveDisplayName: name saved successfully");
                        signupView.navigateToLogin();
                    } else {
                        Log.d(TAG, "saveDisplayName: failed to save name.");
                        if (task.getException() != null) {
                            Log.d(TAG, "saveDisplayName: error: " + task.getException().getMessage());
                        }
                    }
                    signupView.updateButtonState(true);
                }
            });
        }
    }

    /***********************************************************
    *  Overrides from ISignupPresenter
    **********************************************************/
    @Override
    public void validateCredentials(String name, String email, String password) {
        boolean err = false;
        if (name.isEmpty()) {
            signupView.setNameError(context.getResources().getString(R.string.error_name_required));
            err = true;
        }
        if (email.isEmpty()) {
            signupView.setEmailError(context.getResources().getString(R.string.error_email_required));
            err = true;
        }
        if (password.isEmpty()) {
            signupView.setPasswordError(context.getResources().getString(R.string.error_password_required));
            err = true;
        }

        if (!err) {
            createUser(name, email, password);
        } else {
            signupView.updateButtonState(true);
            signupView.setPrbVisible(View.INVISIBLE);
        }
    }
}
