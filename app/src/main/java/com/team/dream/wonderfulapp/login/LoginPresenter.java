package com.team.dream.wonderfulapp.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.team.dream.wonderfulapp.Constants;
import com.team.dream.wonderfulapp.R;

public class LoginPresenter implements ILoginPresenter {

    /***********************************************************
     *  Constants
     **********************************************************/
    private static final String TAG = "LoginPresenter";

    /***********************************************************
     *  Attributes
     **********************************************************/
    private FirebaseAuth mAuth;
    private FirebaseAnalytics firebaseAnalytics;
    private Context context;

    @NonNull
    private ILoginView loginView;

    /***********************************************************
    *  Constructors
    **********************************************************/
    public LoginPresenter(@NonNull ILoginView loginView, Context context) {
        this.loginView = loginView;
        mAuth = FirebaseAuth.getInstance();
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        this.context = context;
    }

    /***********************************************************
    *  Business Methods
    **********************************************************/
    private void auth(final String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        loginView.updateButtonState(true);
                        loginView.setPrbVisible(View.INVISIBLE);
                        if (task.isSuccessful()) {
                            Log.d(TAG, "auth: user authenticated successfully");
                            loginView.navigateToHome();

                            Bundle params = new Bundle();
                            params.putString("email", email);
                            firebaseAnalytics.logEvent(Constants.LOG_EVENT_LOGIN_SUCCESS, params);

                        } else {
                            Log.d(TAG, "auth: failed to authenticate user");
                            if (task.getException() != null) {
                                Log.d(TAG, "auth: error: " + task.getException().getMessage());
                                loginView.showToasty(task.getException().getMessage());
                            }

                            Bundle params = new Bundle();
                            params.putString("email", email);
                            firebaseAnalytics.logEvent(Constants.LOG_EVENT_LOGIN_FAILED, params);

                        }

                    }
                });
    }

    /***********************************************************
     *  Overrides from ILoginPresenter
     **********************************************************/
    @Override
    public void validateCredentials(String email, String password) {
        boolean err = false;
        if (email.isEmpty()) {
            loginView.setEmailError(context.getResources().getString(R.string.error_email_required));
            err = true;
        }
        if (password.isEmpty()) {
            loginView.setPasswordError(context.getResources().getString(R.string.error_password_required));
            err = true;
        }

        if(!err) {
            auth(email, password);
        } else {
            loginView.updateButtonState(true);
            loginView.setPrbVisible(View.INVISIBLE);
        }
    }



    @Override
    public void checkIfUserAlreadyConnected() {
        FirebaseUser user = mAuth.getCurrentUser();
        if(user != null) {
            loginView.navigateToHome();
        }
    }
}
