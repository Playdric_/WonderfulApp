package com.team.dream.wonderfulapp.character;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.team.dream.wonderfulapp.Constants;
import com.team.dream.wonderfulapp.crosslayer.BaseActivity;
import com.team.dream.wonderfulapp.model.Character;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CharacterPresenter implements ICharacterPresenter {

    /***********************************************************
     *  Constants
     **********************************************************/
    private static final String TAG = "CharacterPresenter";

    /***********************************************************
     *  Attributes
     **********************************************************/
    private FirebaseAuth mAuth;
    private FirebaseAnalytics firebaseAnalytics;

    private Context context;

    @NonNull
    private ICharacterView mainView;

    /***********************************************************
     *  Constructors
     **********************************************************/
    public CharacterPresenter(@NonNull ICharacterView v, Context context) {
        mainView = v;
        mAuth = FirebaseAuth.getInstance();
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);

        this.context = context;


    }

    /***********************************************************
     *  Business Methods
     **********************************************************/
    public void getUser() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            Log.d(TAG, user.getDisplayName() + "");
        } else {
            Log.d(TAG, "NULL");
        }
    }

    public void getAllCharacter(final int page) {
        AndroidNetworking.get(Constants.CHARACTER_URL)
                .addQueryParameter("page", page + "")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: response : >" + response.toString());
                        try {
                            JSONArray array = response.getJSONArray(Constants.RESULT_API);
                            JsonParser parser = new JsonParser();
                            JsonElement arr = parser.parse(array.toString());
                            Gson gson = new GsonBuilder().create();
                            Character[] character = gson.fromJson(arr, Character[].class);
                            Log.d(TAG, "onResponse: " + character[0].toString());
                            mainView.printCharacters(character);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.d(TAG, "onError: " + error);
                        if (page == 1) {
                            //Error on the first load
                            //Should replace the recycler view by an error view
                            mainView.setErrorOnFirstLoad();
                        } else {
                            //Error on the n load
                            //Should replace the loader on the bottom of the RV by something to say 'network is fucked up'
                            mainView.setErrorOnNLoad();
                        }
                    }
                });
    }

    @Override
    public ArrayList<Character> favList(List<Character> list) {
        String prefName = FirebaseAuth.getInstance().getCurrentUser().getEmail();

        SharedPreferences prefs = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        ArrayList<Character> characters = new ArrayList<>();
        for (Character character : list) {
            if (prefs.getBoolean(character.getName(), false)) characters.add(character);
        }

        Bundle params = new Bundle();
        params.putString("name_characters", characters.toString());
        firebaseAnalytics.logEvent(Constants.LOG_EVENT_FAV, params);
        return characters;
    }
}
