package com.team.dream.wonderfulapp.character;

import com.team.dream.wonderfulapp.model.Character;

public interface ICharacterView {

    /**
     * Displays the character array into the RecyclerView
     *
     * @param characters the array of characters
     */
    void printCharacters(Character[] characters);

    /**
     * Manage the case when the first download fails
     */
    void setErrorOnFirstLoad();

    /**
     * Manage the case when the n download fails
     */
    void setErrorOnNLoad();
}
