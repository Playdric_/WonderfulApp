package com.team.dream.wonderfulapp.character;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.team.dream.wonderfulapp.R;
import com.team.dream.wonderfulapp.crosslayer.BaseActivity;
import com.team.dream.wonderfulapp.model.Character;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CharacterActivity extends BaseActivity implements ICharacterView {

    /***********************************************************
     *  Constants
     **********************************************************/
    private static final String TAG = "CharacterActivity";

    /***********************************************************
     *  Attributes
     **********************************************************/
    @BindView(R.id.rcv_characters)
    RecyclerView rcvCharacters;

    @BindView(R.id.txv_network_error)
    TextView txvNetworkError;

    private SearchView searchView;

    private int page = 1;

    private List<Character> characterList = new ArrayList<>();

    private CharacterPresenter presenter;
    private CharacterAdapter adapter;

    private boolean shouldLoadMore = true;

    private boolean fav = false;

    /***********************************************************
     *  Managing LifeCycle
     **********************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(R.string.app_name);

        ButterKnife.bind(this);

        presenter = new CharacterPresenter(this, this);
        presenter.getUser();
        presenter.getAllCharacter(page);

        rcvCharacters.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CharacterAdapter(this);
        rcvCharacters.setAdapter(adapter);

    }

    /***********************************************************
     *  Business Methods
     **********************************************************/
    public void increasePage() {
        this.page++;
    }

    /***********************************************************
     *  Managing menus
     **********************************************************/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_sign_out) {
            FirebaseAuth.getInstance().signOut();
            finish();
        }
        if (item.getItemId() == R.id.action_fav) {
            fav = !fav;
            if (fav) {
                item.setIcon(R.drawable.ic_star);
                shouldLoadMore = false;
                adapter.setShouldListenToOnBottomReached(false);
                adapter.resetData(presenter.favList(characterList));
            } else {
                item.setIcon(R.drawable.ic_star_border);
                shouldLoadMore = true;
                adapter.setShouldListenToOnBottomReached(true);
                adapter.resetData(characterList);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);// Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                shouldLoadMore = false;
                adapter.setShouldListenToOnBottomReached(false);
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                shouldLoadMore = false;
                adapter.setShouldListenToOnBottomReached(false);
                // filter recycler view when text is changed
                adapter.getFilter().filter(query);
                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                Log.d(TAG, "onClose");
                shouldLoadMore = true;
                adapter.setShouldListenToOnBottomReached(true);
                return false;
            }
        });
        return true;
    }

    /***********************************************************
     *  Overrides from ICharacterView
     **********************************************************/
    @Override
    public void printCharacters(Character[] characters) {
        if (characterList.size() > 0) {
            adapter.setLoaded();
            characterList.remove(characterList.size() - 1);
            adapter.notifyItemRemoved(characterList.size() - 1);
        }
        characterList.addAll(Arrays.asList(characters));
        adapter.resetData(characterList);
        adapter.setOnBottomReachedListener(new OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                Log.d(TAG, "onBottomReached() called");
                if (shouldLoadMore) {
                    characterList.add(null);
                    rcvCharacters.post(new Runnable() {
                        public void run() {
                            adapter.notifyItemInserted(characterList.size());
                        }
                    });
                    presenter.getAllCharacter(page);
                }
            }
        });
        increasePage();
    }

    @Override
    public void setErrorOnFirstLoad() {
        rcvCharacters.setVisibility(View.GONE);
        txvNetworkError.setVisibility(View.VISIBLE);
    }

    @Override
    public void setErrorOnNLoad() {
        characterList.remove(characterList.size() - 1);
        adapter.notifyItemRemoved(characterList.size() - 1);
        Toast.makeText(this, getResources().getString(R.string.network_error), Toast.LENGTH_SHORT).show();
    }

    /***********************************************************
     *  Overrides from BaseActivity
     **********************************************************/
    @Override
    public int getLayoutId() {
        return R.layout.activity_character;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_character;
    }
}
