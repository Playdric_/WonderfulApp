package com.team.dream.wonderfulapp.login;

public interface ILoginPresenter {

    /**
     * Check if the credentials are not empty
     * @param email - the mail of the dude
     * @param password - the password of the dude
     */
    void validateCredentials(String email, String password);

    /**
     * Checks if the user is already connected
     */
    void checkIfUserAlreadyConnected();
}
