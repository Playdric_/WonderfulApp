package com.team.dream.wonderfulapp.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.team.dream.wonderfulapp.R;
import com.team.dream.wonderfulapp.character.CharacterActivity;
import com.team.dream.wonderfulapp.episode.EpisodeActivity;
import com.team.dream.wonderfulapp.signup.SignupActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity implements ILoginView {

    /***********************************************************
    *  Attributes
    **********************************************************/
    @BindView(R.id.edt_email)
    EditText edtEmail;

    @BindView(R.id.edt_password)
    EditText edtPassword;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @BindView(R.id.btn_signup)
    TextView btnSignup;

    @BindView(R.id.prb_login)
    ProgressBar prbLogin;

    private LoginPresenter presenter;

    private FirebaseAnalytics firebaseAnalytics;

    /***********************************************************
    *  Managing LifeCycle
    **********************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        presenter = new LoginPresenter(this, this);

        setChangeListeners();
    }

    @Override
    protected void onStart() {
        super.onStart();

        firebaseAnalytics.logEvent("NAVIGATION", null);

        presenter.checkIfUserAlreadyConnected();
    }

    /***********************************************************
     *  Managing clicks
     **********************************************************/
    @OnClick(R.id.btn_signup)
    void onClickBtnSignup(View v) {
        startActivity(new Intent(this, SignupActivity.class));
        finish();
    }

    @OnClick(R.id.btn_login)
    void onClickBtnLogin(View v) {
        updateButtonState(false);
        setPrbVisible(View.VISIBLE);
        presenter.validateCredentials(edtEmail.getText().toString(), edtPassword.getText().toString());
    }


    private void setChangeListeners() {
        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                edtEmail.setError(null);
                edtEmail.clearFocus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    /***********************************************************
     *  Overrides from ILoginView
     **********************************************************/
    @Override
    public void setEmailError(String err) {
        edtEmail.setError(err);
    }

    @Override
    public void setPasswordError(String err) {
        edtPassword.setError(err);
    }

    @Override
    public void navigateToHome() {
        startActivity(new Intent(this, CharacterActivity.class));
        finish();
    }

    @Override
    public void updateButtonState(boolean state) {
        btnLogin.setEnabled(state);
        btnSignup.setClickable(state);
    }

    @Override
    public void setPrbVisible(int visibility) {
        prbLogin.setVisibility(visibility);
    }

    @Override
    public void showToasty(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
