package com.team.dream.wonderfulapp.episode;

import com.team.dream.wonderfulapp.model.Episode;

/**
 * Created by Florian on 30/04/2018.
 */

public interface IEpisodeView {
    void printEpisodes(Episode[] episodes);


    void updateMaxPage(int maxPage);
}
