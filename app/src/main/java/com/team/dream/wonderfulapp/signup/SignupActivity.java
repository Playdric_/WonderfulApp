package com.team.dream.wonderfulapp.signup;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.team.dream.wonderfulapp.R;
import com.team.dream.wonderfulapp.login.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupActivity extends AppCompatActivity implements ISignupView {

    /***********************************************************
    *  Attributes
    **********************************************************/
    @BindView(R.id.edt_email)
    EditText edtEmail;

    @BindView(R.id.edt_password)
    EditText edtPassword;

    @BindView(R.id.btn_signup)
    Button btnSignup;

    @BindView(R.id.btn_login)
    TextView txvLogin;

    @BindView(R.id.edt_name)
    TextView edtName;

    @BindView(R.id.prb_signup)
    ProgressBar prbSignup;

    private SignupPresenter presenter;

    /***********************************************************
    *  Managing LifeCycle
    **********************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        presenter = new SignupPresenter(this, this);
    }

    /***********************************************************
     *  Managing clicks
     **********************************************************/
    @OnClick(R.id.btn_login)
    void onClickLogin(View v) {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @OnClick(R.id.btn_signup)
    void onClickSignup(View v) {
        updateButtonState(false);
        setPrbVisible(View.VISIBLE);
        presenter.validateCredentials(edtName.getText().toString(), edtEmail.getText().toString(), edtPassword.getText().toString());
    }

    /***********************************************************
     *  Overrides from ISignupView
     **********************************************************/
    @Override
    public void setNameError(String err) {
        edtName.setError(err);
    }

    @Override
    public void setEmailError(String err) {
        edtEmail.setError(err);
    }

    @Override
    public void setPasswordError(String err) {
        edtPassword.setError(err);
    }

    @Override
    public void navigateToLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void updateButtonState(boolean state) {
        btnSignup.setEnabled(state);
        txvLogin.setClickable(state);
    }

    @Override
    public void setPrbVisible(int visibility) {
        prbSignup.setVisibility(visibility);
    }

    @Override
    public void showToasty(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
