package com.team.dream.wonderfulapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Quote {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("who")
    @Expose
    private String who;

    @SerializedName("what")
    @Expose
    private String what;

    @SerializedName("where")
    @Expose
    private String where;

    @SerializedName("when")
    @Expose
    private String when;

    public Quote() {}

    public Quote(String id, String who, String what, String where, String when) {
        this.id = id;
        this.who = who;
        this.what = what;
        this.where = where;
        this.when = when;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getWhat() {
        return what;
    }

    public void setWhat(String what) {
        this.what = what;
    }

    public String getWhere() {
        return where;
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
    }

    @Override
    public String toString() {
        return "Quote{" +
                "id='" + id + '\'' +
                ", who='" + who + '\'' +
                ", what='" + what + '\'' +
                ", where='" + where + '\'' +
                ", when='" + when + '\'' +
                '}';
    }
}
