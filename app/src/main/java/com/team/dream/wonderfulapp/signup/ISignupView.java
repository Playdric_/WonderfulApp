package com.team.dream.wonderfulapp.signup;

public interface ISignupView {

    /**
     * Set the error in the name TextInputLayout
     * @param err - the error to display
     */
    void setNameError(String err);

    /**
     * Set the error in the email TextInputLayout
     * @param err - the error to display
     */
    void setEmailError(String err);

    /**
     * Set the error in the password TextInputLayout
     * @param err - the error to display
     */
    void setPasswordError(String err);

    /**
     * Navigate to the LoginActivity
     */
    void navigateToLogin();

    /**
     * Update the state of the buttons in the view, when the view is waiting for the answer from
     * firebase.
     * @param state
     */
    void updateButtonState(boolean state);

    void setPrbVisible(int visibility);

    void showToasty(String msg);
}
