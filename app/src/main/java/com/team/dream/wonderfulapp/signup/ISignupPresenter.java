package com.team.dream.wonderfulapp.signup;

public interface ISignupPresenter {
    /**
     * Check if the credentials are not empty
     * @param email - the mail of the dude
     * @param password - the password of the dude
     */
    void validateCredentials(String name, String email, String password);
}
