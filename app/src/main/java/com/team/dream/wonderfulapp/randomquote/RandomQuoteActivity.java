package com.team.dream.wonderfulapp.randomquote;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.team.dream.wonderfulapp.R;
import com.team.dream.wonderfulapp.crosslayer.BaseActivity;
import com.team.dream.wonderfulapp.model.Quote;

import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RandomQuoteActivity extends BaseActivity implements IRandomQuoteView, SensorEventListener {

    /***********************************************************
     *  Constants
     **********************************************************/
    private static final String TAG = "RandomQuoteActivity";
    private static final int SHAKE_THRESHOLD = 5000;

    /***********************************************************
     *  Attributes
     **********************************************************/
    private TextToSpeech tts;
    private int ttsStatus = TextToSpeech.ERROR;
    private RandomQuotePresenter presenter;
    long lastUpdate = 0;
    long lastShake = 0;
    float last_x = 0;
    float last_y = 0;
    float last_z = 0;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    @BindView(R.id.txv_who)
    TextView txvWho;

    @BindView(R.id.txv_what)
    TextView txvRandomQuote;

    private List<Quote> quoteList;

    private FirebaseAnalytics firebaseAnalytics;

    /***********************************************************
     *  Managing LifeCycle
     **********************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(R.string.activity_title_random_quote);

        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS) {
                    tts.setLanguage(Locale.UK);
                    ttsStatus = status;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        tts.setVoice(new Voice("en-us-x-sfg#male_2-local", Locale.US, Voice.QUALITY_VERY_HIGH, Voice.LATENCY_NORMAL, true, null));
                    }
                    speakText(txvRandomQuote.getText().toString());
                }
            }
        });

        ButterKnife.bind(this);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        presenter = new RandomQuotePresenter(this, getApplicationContext());
        quoteList = presenter.loadJsonQuotes();

        updateWithNewQuote();


    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
        tts.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tts.shutdown();
    }

    /***********************************************************
     *  Business Methods
     **********************************************************/
    private void updateWithNewQuote() {
        Random rdm = new Random();
        int i = rdm.nextInt(quoteList.size());
        Quote quote = quoteList.get(i);


        txvRandomQuote.setText(quote.getWhat());
        txvWho.setText(quote.getWho());
        if (ttsStatus == TextToSpeech.SUCCESS) {
            speakText(quote.getWhat());

            Bundle params = new Bundle();
            params.putString("QUOTE_ID", quote.getId());
            firebaseAnalytics.logEvent("QUOTE", params);
        }
    }

    private void speakText(String toSpeak) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.stop();
            tts.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null, null);

        }
    }

    /***********************************************************
     *  Overrides from SensorEventListener
     **********************************************************/
    @Override
    public void onSensorChanged(SensorEvent event) {
        long curTime = System.currentTimeMillis();
        long diffTime;
        // only allow one update every 2000ms.
        if ((diffTime = curTime - lastUpdate) > 100 && (curTime - lastShake) > 2000) {
            lastUpdate = curTime;

            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 100000;

            last_x = x;
            last_y = y;
            last_z = z;

            if (speed > SHAKE_THRESHOLD) {
                Log.d("sensor", "shake detected w/ speed: " + speed);
                lastShake = System.currentTimeMillis();
                updateWithNewQuote();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /***********************************************************
     *  Overrides from BaseActivity
     **********************************************************/
    @Override
    public int getLayoutId() {
        return R.layout.activity_random_quote;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_random_quote;
    }
}
