package com.team.dream.wonderfulapp.episode;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.team.dream.wonderfulapp.Constants;
import com.team.dream.wonderfulapp.crosslayer.BaseActivity;
import com.team.dream.wonderfulapp.model.Episode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Florian on 29/04/2018.
 */

public class EpisodePresenter implements IEpisodePresenter{
    private static final String TAG = "EpisodePresenter";
    private FirebaseAuth mAuth;
    private FirebaseAnalytics firebaseAnalytics;
    private Context context;

    @NonNull
    private IEpisodeView mainView;

    public EpisodePresenter(@NonNull IEpisodeView v, Context context) {
        mainView = v;
        mAuth = FirebaseAuth.getInstance();
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        this.context = context;
    }

    public void getUser() {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            Log.d(TAG, user.getDisplayName() + "");
        } else {
            Log.d(TAG, "NULL");
        }
    }

    public void getAllEpisodes(int page) {
        AndroidNetworking.get(Constants.EPISODE_URL)
                .addQueryParameter(Constants.CURRENT_PAGE_API, page + "")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "onResponse: response : >" + response.toString());
                        try {
                            JSONObject info = response.getJSONObject(Constants.INFO_API);
                            mainView.updateMaxPage(info.getInt(Constants.NUMBER_PAGES_API));
                            JSONArray array = response.getJSONArray(Constants.RESULT_API);
                            JsonParser parser = new JsonParser();
                            JsonElement arr = parser.parse(array.toString());
                            Gson gson = new GsonBuilder().create();
                            Episode[] episode = gson.fromJson(arr, Episode[].class);
                            Log.d(TAG, "onResponse: " + episode[0].toString());
                            mainView.printEpisodes(episode);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.d(TAG, "onError: " + error);
                    }
         });
    }

    @Override
    public ArrayList<Episode> favList(List<Episode> list) {
        String prefName = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        Log.i("prefName", prefName);
        SharedPreferences prefs = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
        ArrayList<Episode> episodes = new ArrayList<>();
        for (Episode episode : list) {
            if (prefs.getBoolean(episode.getName(), false)) episodes.add(episode);
        }

        Bundle params = new Bundle();
        params.putString("name_episodes", episodes.toString());
        firebaseAnalytics.logEvent(Constants.LOG_EVENT_FAV, params);
        return episodes;
    }

}
