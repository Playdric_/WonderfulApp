package com.team.dream.wonderfulapp.character;

public interface OnBottomReachedListener {

    /**
     * When the bottom of the RecyclerView is reached
     */
    void onBottomReached();
}
