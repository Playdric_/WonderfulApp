package com.team.dream.wonderfulapp.episode;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.team.dream.wonderfulapp.R;
import com.team.dream.wonderfulapp.crosslayer.BaseActivity;
import com.team.dream.wonderfulapp.model.Episode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Florian on 29/04/2018.
 */

public class EpisodeActivity extends BaseActivity implements IEpisodeView{
    private static final String TAG = "EpisodeActivity";

    @BindView(R.id.rcv_episodes)
    RecyclerView rcvEpisodes;


    private EpisodePresenter presenter;
    private EpisodeAdapter adapter;
    private int page = 1;
    private int maxPage;
    private List<Episode> episodeList = new ArrayList<>();
    private boolean shouldLoadMore = true;
    private SearchView searchView;
    private boolean fav = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(R.string.app_name);

        presenter = new EpisodePresenter(this, this);

        presenter.getUser();

        presenter.getAllEpisodes(page);

        rcvEpisodes.setLayoutManager(new LinearLayoutManager(this));
        adapter = new EpisodeAdapter(this);
        rcvEpisodes.setAdapter(adapter);

    }

    @Override
    public void printEpisodes(Episode[] episodes) {
        if (episodeList.size() > 0) {
            adapter.setLoaded();
            episodeList.remove(episodeList.size() - 1);
            adapter.notifyItemRemoved(episodeList.size() - 1);
        }
        episodeList.addAll(Arrays.asList(episodes));
        adapter.resetData(episodeList);
        adapter.setOnBottomReachedListener(new OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                Log.d(TAG, "onBottomReached() called");
                if (shouldLoadMore) {
                    episodeList.add(null);
                    rcvEpisodes.post(new Runnable() {
                        public void run() {
                            adapter.notifyItemInserted(episodeList.size());
                        }
                    });
                    presenter.getAllEpisodes(page);
                }
            }
        });
        increasePage();

    }

    @Override
    public void updateMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public void increasePage() {
        if(this.page == this.maxPage) {
            shouldLoadMore = false;
        }
        Log.i("Max Pages : ", String.valueOf(this.maxPage));
        Log.i("Current page : ", String.valueOf(this.page));
        this.page += 1;



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_sign_out) {
            FirebaseAuth.getInstance().signOut();
            finish();
        }

        if (item.getItemId() == R.id.action_fav) {
            fav = !fav;
            if (fav) {
                item.setIcon(R.drawable.ic_star);
                shouldLoadMore = false;
                adapter.setShouldListenToOnBottomReached(false);
                adapter.resetData(presenter.favList(episodeList));
            } else {
                item.setIcon(R.drawable.ic_star_border);
                shouldLoadMore = true;
                adapter.setShouldListenToOnBottomReached(true);
                adapter.resetData(episodeList);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                shouldLoadMore = false;
                adapter.setShouldListenToOnBottomReached(false);
                // filter recycler view when query submitted
                adapter.getFilter().filter(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                shouldLoadMore = false;
                adapter.setShouldListenToOnBottomReached(false);
                // filter recycler view when text is changed
                adapter.getFilter().filter(newText);

                return false;
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                Log.d(TAG, "onClose");
                shouldLoadMore = true;
                adapter.setShouldListenToOnBottomReached(true);
                return false;
            }
        });
        return true;
    }

    /***********************************************************
     *  Overrides from BaseActivity
     **********************************************************/
    @Override
    public int getLayoutId() {
        return R.layout.activity_episode;
    }

    @Override
    public int getNavigationMenuItemId() {
        return R.id.navigation_episode;
    }
}
