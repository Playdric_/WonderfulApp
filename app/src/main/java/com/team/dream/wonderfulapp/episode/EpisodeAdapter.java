package com.team.dream.wonderfulapp.episode;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.team.dream.wonderfulapp.R;
import com.team.dream.wonderfulapp.model.Episode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Florian on 29/04/2018.
 */

public class EpisodeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    private static final String TAG = "EpisodeAdapter";

    private List<Episode> data = new ArrayList<>();
    private List<Episode> dataFiltered = new ArrayList<>();
    private Activity activity;
    private OnBottomReachedListener onBottomReachedListener;
    private boolean isLoading;
    private boolean shouldListenToOnBottomReached = true;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;


    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener) {
        this.onBottomReachedListener = onBottomReachedListener;
    }

    public void setShouldListenToOnBottomReached(boolean b) {
        shouldListenToOnBottomReached = b;
    }


    public void setLoaded() {
        isLoading = false;
    }

    @Override
    public int getItemViewType(int position) {
        if(dataFiltered.get(position) != null){
            return VIEW_TYPE_ITEM;
        }
        else {
            return VIEW_TYPE_LOADING;
        }
    }

    public EpisodeAdapter(Activity activity) {
        this.activity = activity;
        RecyclerView mRecyclerView = ((EpisodeActivity) activity).rcvEpisodes;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold) && shouldListenToOnBottomReached) {
                    if (onBottomReachedListener != null) {
                        onBottomReachedListener.onBottomReached();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void resetData(List<Episode> items) {
        this.dataFiltered = items;
        this.data = items;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_episode, parent, false);
        if(viewType == VIEW_TYPE_ITEM){
            View v = LayoutInflater.from(activity.getBaseContext()).inflate(R.layout.item_episode, parent, false);
            return new EpisodeAdapter.EpisodeViewHolder(v);
            //return new EpisodeAdapter.ViewHolder(v);
        }
        else{
            View v = LayoutInflater.from(activity.getBaseContext()).inflate(R.layout.loading_item, parent, false);
            return new EpisodeAdapter.LoadingViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if(holder.getItemViewType() == VIEW_TYPE_ITEM){
            final EpisodeAdapter.EpisodeViewHolder viewHolder = (EpisodeAdapter.EpisodeViewHolder) holder;
            //Shared Preferences editor, to put the boolean when checked
            String prefName = FirebaseAuth.getInstance().getCurrentUser().getEmail();
            final SharedPreferences.Editor editor = activity.getSharedPreferences(prefName, Context.MODE_PRIVATE).edit();
            final String titleEpisode = dataFiltered.get(position).getName();
            final String seasonEpisode = dataFiltered.get(position).getEpisode();

            viewHolder.txvEpisodeTitle.setText(titleEpisode);
            viewHolder.txvEpisodeNumber.setText(seasonEpisode);

            //Retrieving the state (checked) for the current item
            SharedPreferences prefs = activity.getSharedPreferences(prefName, Context.MODE_PRIVATE);
            boolean checked = prefs.getBoolean(titleEpisode, false);
            viewHolder.ckbEpisode.setChecked(checked);
            viewHolder.ckbEpisode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean checked = ((CheckBox) v).isChecked();
                    editor.putBoolean(dataFiltered.get(position).getName(), checked);
                    editor.apply();
                    Log.d(TAG, checked ? "onClick: checked " : "onClick: unchecked " + titleEpisode);
                }
            });
        }
        else if(holder.getItemViewType() == VIEW_TYPE_LOADING){
            final EpisodeAdapter.LoadingViewHolder loadingViewHolder = (EpisodeAdapter.LoadingViewHolder) holder;
            if (loadingViewHolder.progressBar == null) {
            } else {
                loadingViewHolder.progressBar.setIndeterminate(true);
            }

        }


    }

    @Override
    public int getItemCount() {

        return dataFiltered == null ? 0 : dataFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();
                if(charString.isEmpty()){
                    dataFiltered = data;
                }
                else {
                    List<Episode> filteredList = new ArrayList<>();
                    for (Episode row : data) {
                        if (row != null && row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    dataFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = dataFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                dataFiltered = (ArrayList<Episode>) results.values;
                notifyDataSetChanged();
            }
        };
    }



    class EpisodeViewHolder extends RecyclerView.ViewHolder {

        TextView txvEpisodeTitle;
        TextView txvEpisodeNumber;
        CheckBox ckbEpisode;

        EpisodeViewHolder(View itemView) {
            super(itemView);
            txvEpisodeTitle = itemView.findViewById(R.id.txv_episode_title);
            txvEpisodeNumber = itemView.findViewById(R.id.txv_episode_number);
            ckbEpisode = itemView.findViewById(R.id.ckb_episode);
        }
    }
    class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.rcvProgressBar);
        }
    }

}
