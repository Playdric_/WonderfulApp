package com.team.dream.wonderfulapp;

/**
 * Created by Florian on 19/07/2018.
 */

public final class Constants {
    public static final String BASE_URL = "https://rickandmortyapi.com/api/";
    public static final String EPISODE_URL = BASE_URL + "episode";
    public static final String CHARACTER_URL = BASE_URL + "character";
    public static final String CURRENT_PAGE_API = "page";
    public static final String INFO_API = "info";
    public static final String NUMBER_PAGES_API = "pages";
    public static final String RESULT_API = "results";
    public static final String LOG_EVENT_FAV = "FAV";
    public static final String LOG_EVENT_LOGIN_SUCCESS ="LOGIN_SUCCESS";
    public static final String LOG_EVENT_LOGIN_FAILED = "LOGIN_FAILED";
    public static final String LOG_EVENT_SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
    public static final String LOG_EVENT_SIGNUP_FAILED ="SIGNUP_FAILED";
    public static final String LOG_EVENT_NAVIGATION = "NAVIGATION";
    public static final String LOG_EVENT_PLAYING_QUOTE = "QUOTE";
    public static final String QUOTE_JSON_FILE = "rick_and_morty.json";
    public static final String PARAMETERS_JSON_ARRAY_QUOTES = "quotes";
    public static final String PARAMETERS_JSON_QUOTE_ID = "id";
    public static final String PARAMETERS_JSON_QUOTE_WHAT = "what";
    public static final String PARAMETERS_JSON_QUOTE_WHERE = "where";
    public static final String PARAMETERS_JSON_QUOTE_WHEN = "when";
    public static final String PARAMETERS_JSON_QUOTE_WHO = "who";


}
