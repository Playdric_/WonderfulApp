package com.team.dream.wonderfulapp.randomquote;

import com.team.dream.wonderfulapp.model.Quote;

import java.util.List;

public interface IRandomQuotePresenter {

    /**
     * Load the json file from the assets. In this file, there are the quotes
     *
     * @return the List containing all the quotes
     */
    List<Quote> loadJsonQuotes();
}
