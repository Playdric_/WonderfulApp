package com.team.dream.wonderfulapp.episode;

import com.team.dream.wonderfulapp.model.Episode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Florian on 30/04/2018.
 */

public interface IEpisodePresenter {
    void getUser();

    ArrayList<Episode> favList(List<Episode> list);
}
